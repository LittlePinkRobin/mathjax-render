var express = require('express');
var errorHandler = require('errorhandler')
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var app = express();

app.use(errorHandler({dumpExceptions: true, showStack: true }));

const {mathjax} = require('mathjax-full/js/mathjax.js');
const {TeX} = require('mathjax-full/js/input/tex.js');
const {SVG} = require('mathjax-full/js/output/svg.js');
const {liteAdaptor} = require('mathjax-full/js/adaptors/liteAdaptor.js');
const {RegisterHTMLHandler} = require('mathjax-full/js/handlers/html.js');
const {AssistiveMmlHandler} = require('mathjax-full/js/a11y/assistive-mml.js');

const {AllPackages} = require('mathjax-full/js/input/tex/AllPackages.js');

//
//  Create DOM adaptor and register it for HTML documents
//
const adaptor = liteAdaptor();
const handler = RegisterHTMLHandler(adaptor);

//
//  Create input and output jax and a document using them on the content from the HTML file
//
const tex = new TeX({packages:AllPackages,
                      macros: {
                        B: "{\\mathbb{B}}",
                        C: "{\\mathbb{C}}",
                        K: "{\\mathbb{K}}",
                        R: "{\\mathbb{R}}",
                        Q: "{\\mathbb{Q}}",
                        Z: "{\\mathbb{Z}}",
                        Alpha: "{A}",
                        Eta: "{E}",
                        Epsilon: "{E}",
                        Iota: "{\\iota}",
                        Mu: "{\\mathrm{M}}",
                        Nu: "{\\mathrm{N}",
                        Omicron: "{\\mathrm{O}}",
                        omicron: "{\\mathrm{o}}",
                        PI: "{\\Pi}",
                        Rho: "{\\rho}",
                        Tau: "{\\tau}",
                        Chi: "X",
                        real: "{\\mathbb{R}}",
                        reals: "{\\mathbb{R}}",
                        Complex: "{\\mathbb{C}}",
                        Zeta: "{\\mathbb{Z}}",
                        alef: "{\\aleph}",
                        ang: "{\\angle}",
                        bull: "{\\bullet}",
                        darr: "{\\downarrow}",
                        Dagger: "{\\dagger}",
                        exist: "{{\\exists}",
                        isin: "{\\in}",
                        Larr: "{\\Leftarrow}",
                        lrarr: "{\\leftrightarrow}",
                        plusmn: "{\\pm}",
                        Rarr: "{\\Rightarrow}",
                        sdot: "{\\cdot}",
                        sub: "{\\subset}",
                        sube: "{\\subseteq}",
                        supe: "{\\supseteq}",
                        uarr: "{\\uparrow}",
                        lt: "{<}",
                        gt: "{<}",
                        Cos: "{\\cos}",
                        Sin: "{\\sin}",
                        Tan: "{\\tan}",
                        arcsec: "{arcsec}",
                        arccot: "{arccot}",
                        arccsc: "{arccsc}"
                         }}) 
const svg = new SVG({fontCache: 'none'});



app.get('/render/:latex', function (req, res){
  var html = mathjax.document('', {InputJax: tex, OutputJax: svg});
  var node = html.convert(req.params.latex);
  res.send(adaptor.outerHTML(node));
  
  })
  
app.get('/render/', jsonParser, function (req, res){
  var html = mathjax.document('', {InputJax: tex, OutputJax: svg});
  try{
    var node = html.convert(req.body.latex);
    res.send(adaptor.outerHTML(node));
  } catch(err) {
    res.status(400).json({error: err.toString() });
  }
  
  })

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})
