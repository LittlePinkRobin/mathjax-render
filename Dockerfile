FROM node:12.21.0-alpine3.12

COPY ./ ./

RUN npm install


CMD [ "npm", "start" ]