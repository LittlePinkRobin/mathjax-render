First make sure you have docker installed and swarm enabled.

to check if you have docker installed and that swarm mode is activated run "docker info" and check to see that the swarm field is set to "active"

simply run: docker stack deploy mathjax-render --compose-file docker-compose.yml

to increase the amount of containers running alter the replicas field in the docker-compose file

similarily if port 8123 is taken on your machine change the port specified in the publish field
